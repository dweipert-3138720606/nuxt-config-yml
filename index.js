const yaml = require('js-yaml');
const path = require('path');
const fs = require('fs');

module.exports = function NuxtConfigYml (moduleOptions) {
  // default module options
  const defaultOptions = {
    configFile: path.resolve(this.options.rootDir, 'nuxt.config.yml')
  };

  // assign custom options
  moduleOptions = Object.assign(defaultOptions, this.options.nuxtConfigYml, moduleOptions);

  let yamlConfig = {};
  // check if file exists
  if (fs.existsSync(moduleOptions.configFile)) {
    // read yaml config file
    yamlConfig = yaml.load(fs.readFileSync(moduleOptions.configFile, 'utf8'));
  }

  // merge yaml config into nuxt config
  this.options = Object.assign(this.options, yamlConfig);
};

module.exports.meta = require('./package.json');
